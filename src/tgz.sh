#!/bin/sh
# tgztools multicall binary
# A tool for creating and extracting tgz archives
# tgztools is free software distributed under 3-clause BSD license

# variables
COMPRESSION_LEVEL=${COMPRESSION_LEVEL:-9} # customize compression level by adding 'COMPRESSION_LEVEL=level tgz actions...'
VERBOSE=${VERBOSE:-no} # apply 'VERBOSE=yes tgz ...' to use verbose mode
ACTION=$1
TARBALL="$2"
FILES_IF_ANY=$(echo $* | sed "s/$1 //" | sed "s/$2 //")
UTIL=gzip # look at txz, tbz, tlz etc
CATUTIL=zcat
TAR=${TAR:-tar} # use 'TAR=bsdtar tgz' if you want to use bsdtar instead of gnu/busybox tar
SELFVERSION=1.1

case $0 in
	*tgz*) # tgz (tar + gzip)
		export NAME=tgz
		export UTIL=gzip
		export CATUTIL=zcat
		export UTILFLAGS="f${COMPRESSION_LEVEL}c"
		;;
	*txz*) # txz (tar + xz)
		export NAME=txz
		export UTIL=xz
		export CATUTIL=xzcat
		export UTILFLAGS="zf${COMPRESSION_LEVEL}c"
		;;
	*tbz*) # tbz/tbz2 (tar + bzip2)
		export NAME=tbz
		export UTIL=bzip2
		export CATUTIL=bzcat
		export UTILFLAGS="f${COMPRESSION_LEVEL}c"
		;;
	*tlz*) # tlz (tar + lzip)
		export NAME=tlz
		export UTIL=lzip
		export CATUTIL=lzcat
		export UTILFLAGS="f${COMPRESSION_LEVEL}c"
		;;
	*tlo*) # tlo (tar + lzop)
		export NAME=tlo
		export UTIL=lzop
		export CATUTIL=lzopcat
		export UTILFLAGS="fc"
		;;
	*tpz*) # tpz (tar + ncompress)
		export NAME=tpz
		export UTIL=compress
		export CATUTIL=zcat
		export UTILFLAGS="f"
		;;
	*)
		echo "Error: bad file name."
		echo "Following file names are allowed:"
		echo "tgz (gzip)"
		echo "txz (xz)"
		echo "tbz (bzip2)"
		echo "tlz (lzip)"
		echo "tlo (lzop)"
		echo "tpz (ncompress)"
		exit 1
		;;
esac

case $VERBOSE in
	yes)
		export TARFLAGS="cvf"
		export EXFLAGS="xvf"
		;;
	*)
		export TARFLAGS="cf"
		export EXFLAGS="xf"
		;;
esac

mkarchive()
{
	ALL_NO_ONE=$(echo $* | sed "s/$1 //")
	if [ "$NAME" == "tlz" ]
	then
		$TAR $TARFLAGS tlz.tar $ALL_NO_ONE
		$UTIL -$UTILFLAGS tlz.tar > "$1"
		rm -f tlz.tar
	else
		$TAR $TARFLAGS - $ALL_NO_ONE | $UTIL -$UTILFLAGS > "$1"
	fi
}
exarchive()
{
	TB="$1"
	PLACE="$2"
	if [ ! "$PLACE" == "" ]; then
		if [ "$NAME" == "tlz" ]
		then
			$UTIL -kd "$TB"
			$TAR $EXFLAGS ${TB%.*}.tar -C "$PLACE"
			rm -f ${TB%.*}.tar
		else
			$CATUTIL "$TB" | $TAR $EXFLAGS - -C "$PLACE"
		fi
	else
		if [ "$NAME" == "tlz" ]
		then
			$UTIL -kd "$TB"
			$TAR $EXFLAGS ${TB%.*}.tar
			rm -f ${TB%.*}.tar
		else
			$CATUTIL "$TB" | $TAR $EXFLAGS -
		fi
	fi
}
lkarchive()
{
	TB="$1"
	mkdir -p /tmp/${UTIL}random2
	if [ "$NAME" = "tlz" ]
	then
		$UTIL -kd "$TB"
		$TAR xvf ${TB%.*}.tar -C /tmp/${UTIL}random2
		rm -f ${TB%.*}.tar
	else
		$CATUTIL "$TB" | $TAR xvf - -C /tmp/${UTIL}random2
	fi
	rm -rf /tmp/${UTIL}random2
}
util_help()
{
	echo "Help of $NAME"
	echo "Usage: $NAME {command} {tarball} {files...}"
	echo "Commands:"
	echo "c - create $UTIL archive"
	echo "x - extract $UTIL archive"
	echo "l - view contents of $UTIL archive"
	echo "h - display this help"
	echo "v - display version and exit"
}
case "$1" in
	c)
		mkarchive "$TARBALL" "$FILES_IF_ANY"
		;;
	x)
		exarchive "$TARBALL" "$FILES_IF_ANY"
		;;
	l)
		lkarchive "$TARBALL"
		;;
	v)
		echo "$NAME v$SELFVERSION"
		echo "$NAME is part of tgztools package"
		echo ""
		echo "Project website: http://codeberg.org/glowiak/tgztools"
		exit 0
		;;
	*)
		util_help
		;;
esac
