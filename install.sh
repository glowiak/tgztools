#!/bin/sh
# install script for tgztools
NAME="tgztools installer"
PRODUCT=tgztools
VERSION="0.1"
SRCDIR="src/"
SRCFILE=tgz.sh
PROGRAMS_ALL="tgz txz tbz tlz tlo tpz"
PREFIX=${PREFIX:-/usr/local}
DESTDIR=${DESTDIR:-/}
OPTDIR=${OPTDIR:-/opt}
NEEDS_BASE="${DESTDIR}$PREFIX/bin/tar"
NEEDS_TGZ="${DESTDIR}$PREFIX/bin/gzip ${DESTDIR}/bin/zcat"
NEEDS_TXZ="${DESTDIR}$PREFIX/bin/xz ${DESTDIR}$PREFIX/bin/xzcat"
NEEDS_TBZ="${DESTDIR}$PREFIX/bin/bzip2 ${DESTDIR}$PREFIX/bin/bzcat"
NEEDS_TLZ="${DESTDIR}$PREFIX/bin/lzip"
NEEDS_TLO="${DESTDIR}/bin/lzop ${DESTDIR}$PREFIX/bin/lzopcat"
NEEDS_TPZ="${DESTDIR}$PREFIX/bin/compress ${DESTDIR}/bin/zcat"

if [ ! -d "$SRCDIR" ]; then
	echo "---> Source directory cannot be found!"
	exit 1
fi

configure_self()
{
	echo "Starting configuration..."
	for checks in tar gzip zcat xz xzcat bzip2 bzcat lzip lzop lzopcat compress
	do
		echo -n "Checking for $checks..."
		if [ -f "$(which $checks)" ];
		then
			echo yes
			PROGS_FOUND="$PROGS_FOUND $checks"
			if [ "$checks" == "gzip" ]
			then
				COMPONENTS="$COMPONENTS tgz"
				echo "Installing component: tgz"
			fi
			if [ "$checks" == "xz" ]
			then
				COMPONENTS="$COMPONENTS txz"
				echo "Installing component: txz"
			fi
			if [ "$checks" == "bzip2" ]
			then
				COMPONENTS="$COMPONENTS tbz"
				echo "Installing component: tbz"
			fi
			if [ "$checks" == "lzip" ]
			then
				COMPONENTS="$COMPONENTS tlz"
				echo "Installing component: tlz"
			fi
			if [ "$checks" == "lzop" ]
			then
				COMPONENTS="$COMPONENTS tlo"
				echo "Installing component: tlo"
			fi
			if [ "$checks" == "compress" ]
			then
				COMPONENTS="$COMPONENTS tpz"
				echo "Installing component: tpz"
			fi
		else
			echo no
		fi
	done
	echo "Programs found: $PROGS_FOUND"
	echo "Components to be installed: $COMPONENTS"
	echo "Installation prefix: $PREFIX"
	echo "Installation destdir: $DESTDIR"
	echo "OPT directory: $OPTDIR"
	echo -n "Is that correct? "
	read q1
	if [ "$q1" == "y" ]
	then
		echo "Starting Installation..."
		echo "Creating directories..."
		mkdir -pv $DESTDIR$PREFIX/bin
		mkdir -pv $DESTDIR$OPTDIR/tgztools
		echo "Installing Components..."
		cp -v "$SRCDIR/tgz.sh" $DESTDIR$OPTDIR/tgztools/tarballarchiver
		chmod -v 775 $DESTDIR$OPTDIR/tgztools/tarballarchiver
		for program in $COMPONENTS
		do
			echo "Installing $program..."
			ln -sv $OPTDIR/tgztools/tarballarchiver $DESTDIR$PREFIX/bin/$program
		done
	else
		echo "Abort."
		exit 1
	fi
}
configure_self
