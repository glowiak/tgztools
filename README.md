# tgztools

Tools for creating and extracting TGZ (tar+gzip) archives, more customizable than -czf tar option

# Supported Formats

tgztools currently supports those archive formats:

	tgz (gzip)
	txz (xz)
	tbz (bzip2)
	tlz (lzip)
	tlo (lzop)
	tpz (ncompress)

The ncompress one is bit broken.

# Installation

First grab latest release from [releases](https://codeberg.org/glowiak/tgztools/releases) page. Then unpack it, go to extracted directory and type those commands to install tgztools.

Using install.sh:

	PREFIX=prefix DESTDIR=destdir OPTDIR=optdir ./install.sh

Using Makefile (deprecated):

	make PREFIX=prefix DESTDIR=destdir OPTDIR=optdir

Script will detect installed archivers, and make list of tools that can be installed. If you agree with that, press y and hit enter. This will install tgztools to your system.

Example install commands:

	PREFIX=/usr ./install.sh # this command will install install tgztools to /usr prefix, rest are defaults,
	PREFIX=/usr DESTDIR="$pkgdir" OPTDIR=/usr ./install.sh # this is command from Alpine APKBUILD, where /opt cannot be used for packages
	make PREFIX=/usr # the same as first, but using make
	make PREFIX=/usr DESTDIR=$pkgdir OPTDIR=/usr # the same as second, but using make
	PREFIX=/usr OPTDIR=/var/opt ./install.sh # suitable for most users

# Alpine Linux Packages

I'm providing packages for Alpine Linux [here](http://codeberg.org/glowiak/apkbuilds).

Please have on mind that /usr/bin/tgz file on Alpine (and on many other distros I think) is owned by mtools package. If you have installed mtools, and want tgztools, please move /usr/bin/tgz to /usr/bin/tgz.mtools before installing, add repo and run 'apk add --force-overwrite tgztools' as root. --force-overwrite option tells apk that all files MUST be installed.
