PREFIX=	/usr/local
DESTDIR=	/
OPTDIR=	/opt
all:
	@sh -c 'PREFIX=${PREFIX} DESTDIR=${DESTDIR} OPTDIR=${OPTDIR} ./install.sh'
